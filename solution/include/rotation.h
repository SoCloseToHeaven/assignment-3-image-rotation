//
// Created by Дмитрий on 14.11.2023.
//

#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H
#include "stddef.h"
#include "stdint.h"
struct image rotate(struct image source_img_struct, uint8_t rotations);
#endif //IMAGE_TRANSFORMER_ROTATION_H
