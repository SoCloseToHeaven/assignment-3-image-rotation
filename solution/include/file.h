//
// Created by Дмитрий on 16.11.2023.
//

#ifndef IMAGE_TRANSFORMER_FILE_H
#define IMAGE_TRANSFORMER_FILE_H
#include <stdio.h>
#include <unistd.h>
#define READ_MODE "rb"
#define WRITE_MODE "wb"
#define SOURCE_FILE 1
#define TARGET_FILE 2
#define ANGLE 3
#define NO_ACCESS (-1)


struct argv_files {
    FILE* source;
    FILE* out;
};


enum manage_status {
    MANAGE_OK = 0,
    MANAGE_ERROR
};

enum manage_status manage_files(char** argv, struct argv_files* files);
FILE* open_file_and_check(char* str_file, const char* mode, int access_mode);
void close_files(struct argv_files* files);

#endif //IMAGE_TRANSFORMER_FILE_H
