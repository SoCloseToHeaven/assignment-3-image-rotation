//
// Created by Дмитрий on 14.11.2023.
//

#ifndef IMAGE_TRANSFORMER_VALIDATION_UTILS_H
#define IMAGE_TRANSFORMER_VALIDATION_UTILS_H

#define MAX_ANGLE 360
#define MIN_ANGLE (-360)
#define ANGLE_MULTIPLICITY 90
enum valid_status{
    VALIDATION_OK = 0,
    VALIDATION_INVALID_ARGS = 1,
    VALIDATION_INVALID_ANGLE = 2
};

enum valid_status validate_command_line_args(const int argc);

enum valid_status validate_rotate_angle(const int angle);

void print_error(const char* error_message);
#endif //IMAGE_TRANSFORMER_VALIDATION_UTILS_H
