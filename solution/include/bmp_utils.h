//
// Created by Дмитрий on 16.11.2023.
//

#ifndef IMAGE_TRANSFORMER_BMP_UTILS_H
#define IMAGE_TRANSFORMER_BMP_UTILS_H
#include "bmp.h"
uint64_t get_padding(struct image const* img);
struct bmp_header bmp_header_for_img(struct image const* img);
enum read_status read_bmp_header(FILE* in, struct bmp_header* header);
#endif //IMAGE_TRANSFORMER_BMP_UTILS_H
