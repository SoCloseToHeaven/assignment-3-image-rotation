//
// Created by Дмитрий on 14.11.2023.
//
#include "stdint.h"

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H


struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};


void free_image_data(struct image* img);
struct image copy_image(struct image* source);
struct pixel* malloc_pixels(uint64_t width, uint64_t height);
struct image malloc_image(uint64_t width, uint64_t height);
#endif //IMAGE_TRANSFORMER_IMAGE_H
