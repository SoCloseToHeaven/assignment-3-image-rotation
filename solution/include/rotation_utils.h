//
// Created by Дмитрий on 14.11.2023.
//
#include "stdint.h"

#ifndef IMAGE_TRANSFORMER_ROTATION_UTILS_H
#define IMAGE_TRANSFORMER_ROTATION_UTILS_H
uint8_t get_rotations_count(int angle);
struct image malloc_rotated_image(uint64_t width, uint64_t height);
void rotate_right(struct image* const source, struct image* out);
#endif //IMAGE_TRANSFORMER_ROTATION_UTILS_H
