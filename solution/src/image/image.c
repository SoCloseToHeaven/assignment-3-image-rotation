//
// Created by Дмитрий on 14.11.2023.
//

#include "image.h"
#include "stddef.h"
#include "stdlib.h"

void free_image_data(struct image* img){
    free(img -> data);
    img -> data = NULL;
}


struct pixel* malloc_pixels(uint64_t width, uint64_t height) {
    struct pixel* pixels = (struct pixel*) malloc(width * height * sizeof(struct pixel));
    return pixels;
}

struct image malloc_image(uint64_t width, uint64_t height) {
    struct image out = {0};
    out.width = width;
    out.height = height;
    out.data = malloc_pixels(width, height);
    return out;
}

struct image copy_image(struct image* source) {
    struct image out = malloc_image(source->width, source->height);
    if (out.data == NULL)
        return out;
    for (uint64_t i = 0; i < source->width*source->height; ++i) {
        out.data[i] = source->data[i];
    }
    return out;
}


