//
// Created by Дмитрий on 14.11.2023.
//

#include "validation_utils.h"
#include <stdio.h>

#define ARGS_COUNT 4

enum valid_status validate_command_line_args(const int argc) {
    if (argc == ARGS_COUNT)
        return VALIDATION_OK;
    return VALIDATION_INVALID_ARGS;
}

enum valid_status validate_rotate_angle(const int angle) {
    if (angle % ANGLE_MULTIPLICITY == 0 && angle < MAX_ANGLE && angle > MIN_ANGLE)
        return VALIDATION_OK;
    return VALIDATION_INVALID_ANGLE;
}

void print_error(const char* error_message) {
    fprintf(stderr, "This error occurred: %s", error_message);
}
