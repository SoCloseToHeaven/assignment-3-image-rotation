//
// Created by Дмитрий on 14.11.2023.
//

#include "image.h"
#include "rotation_utils.h"
#include "validation_utils.h"

#define IMAGE_SIDES 4


uint8_t get_rotations_count(int angle){
    int rotations = ((angle + MAX_ANGLE) % MAX_ANGLE) / ANGLE_MULTIPLICITY;
    rotations = (IMAGE_SIDES - rotations) % IMAGE_SIDES;
    return rotations;
}



struct image malloc_rotated_image(uint64_t width, uint64_t height) {
    return malloc_image(height, width);
}

void rotate_right(struct image* const source, struct image* out) {
    for (uint64_t i = 0; i < source->height; i++) {
        for (uint64_t j = 0; j < source->width; j++) {
            out->data[j * out->width + (out->width - 1 - i)] = source->data[i * source->width + j];
        }
    }
}


