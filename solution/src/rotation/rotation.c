//
// Created by Дмитрий on 14.11.2023.
//

#include "image.h"
#include "rotation.h"
#include "rotation_utils.h"

static struct image rotate_once(struct image* const source) {
    struct image out = malloc_rotated_image(source->width, source->height);
    if (out.data == NULL)
        return out;
    rotate_right(source, &out);
    return out;
}

struct image rotate(struct image source, const uint8_t rotations){
    for (uint8_t r = 0; r < rotations; r++){
        struct image rotated_image = rotate_once(&source);
        free_image_data(&source);
        source = rotated_image;
    }
    return source;
}
