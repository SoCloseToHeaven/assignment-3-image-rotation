//
// Created by Дмитрий on 16.11.2023.
//

#include "bmp.h"
#include "bmp_utils.h"





enum read_status from_bmp(FILE* source, struct image* img){
    struct bmp_header header = {0};
    enum read_status status = read_bmp_header(source, &header);

    if(status){
        return status;
    }

    *img = malloc_image(header.biWidth, header.biHeight);

    if(!img->data){
        free_image_data(img);
        return READ_INVALID_BITS;
    }

    uint32_t row_padding = get_padding(img);
    for (uint64_t i = 0; i < img->height; ++i) {
        if (fread(&img->data[i * img->width], sizeof(struct pixel), img->width, source) != img->width) {
            free_image_data(img);
            return READ_INVALID_BITS;
        }
        if(fseek(source, (long) row_padding, SEEK_CUR)!=0){
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;

}

enum write_status to_bmp(FILE* target, struct image const* img) {
    struct bmp_header header = bmp_header_for_img(img);
    uint32_t row_padding = get_padding(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, target) != 1) {
        return WRITE_ERROR;
    }
    for (uint32_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, target) != img->width) {
            return WRITE_ERROR;
        }


        uint8_t padding_bytes[] = {0, 0, 0, 0};
        if (fwrite(&padding_bytes, 1, row_padding, target) != row_padding)
            return WRITE_ERROR_PADDING;
    }
    return WRITE_OK;
}

