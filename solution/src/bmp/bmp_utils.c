//
// Created by Дмитрий on 16.11.2023.
//
#include "bmp_utils.h"

#define PADDING_SIZE 4
#define BF_RESERVED 0
#define BI_COMPRESSION 0
#define BI_SIZE_IMAGE 0
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0
#define B_FTYPE 0x4D42
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define X_PXLS_PER_METER 1000
#define Y_PXLS_PER_METER 1000


uint64_t get_padding(struct image const* img){
    uint64_t row_count_pixels = img->width * sizeof(struct pixel);
    return (PADDING_SIZE - (row_count_pixels % PADDING_SIZE)) % PADDING_SIZE;
}

struct bmp_header bmp_header_for_img(struct image const* img){
    struct bmp_header header = {0};
    uint32_t row_padding = get_padding(img);
    header.bfType = B_FTYPE;
    header.bfileSize = (row_padding + img->width * sizeof(struct pixel)) * img->height + sizeof(struct bmp_header);
    header.bfReserved = BF_RESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BI_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_BIT_COUNT;
    header.biCompression = BI_COMPRESSION;
    header.biSizeImage =BI_SIZE_IMAGE;
    header.biXPelsPerMeter = X_PXLS_PER_METER;
    header.biYPelsPerMeter = Y_PXLS_PER_METER;
    header.biClrUsed = BI_CLR_USED;
    header.biClrImportant = BI_CLR_IMPORTANT;
    return header;
}

enum read_status read_bmp_header(FILE* in, struct bmp_header* header){
    if(fread(&header->bfType, sizeof(uint16_t), 1, in) != 1 || header->bfType != B_FTYPE){
        return READ_INVALID_SIGNATURE;
    }
    size_t header_size = sizeof(struct bmp_header) - sizeof(uint16_t);
    if(fread(((uint8_t*) header) + sizeof(uint16_t), header_size, 1, in) != 1){
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}
