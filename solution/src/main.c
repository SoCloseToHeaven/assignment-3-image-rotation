#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotation.h"
#include "rotation_utils.h"
#include "string.h"
#include "validation_utils.h"
#include <stdlib.h>

int main( int argc, char** argv ) {
    if (validate_command_line_args(argc)) {
        print_error("Invalid args, should be: <input image> <transformed-image> <angle>");
        return 1;
    }

    char* end_pointer = 0;
    int angle = (int) strtol(argv[ANGLE], &end_pointer, 10);
    if (validate_rotate_angle(angle) || strlen(end_pointer) != 0) {
        print_error("Invalid angle, it should be in range (-360, 360) and be divisible by 90");
        return 1;
    }

    struct argv_files files = {0};
    if (manage_files(argv, &files)) {
        print_error("Error occurred while managing files");
        return 1;
    }

    struct image source_image = {0};

    if (from_bmp(files.source, &source_image)) {
        print_error("Error occurred while reading source file");
        return 1;
    }

    struct image out_image = rotate(source_image, get_rotations_count(angle));

    if (to_bmp(files.out, &out_image)) {
        free_image_data(&out_image);
        print_error("Error occurred while writing to output file");
        return 1;
    }

    close_files(&files);
    free_image_data(&out_image);

    return 0;
}
