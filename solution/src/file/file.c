//
// Created by Дмитрий on 16.11.2023.
//

#include "file.h"


enum manage_status manage_files(char** argv, struct argv_files* files) {
    files->source = open_file_and_check(argv[SOURCE_FILE], READ_MODE, R_OK);
    files->out = open_file_and_check(argv[TARGET_FILE], WRITE_MODE, W_OK);
    if (files->source == NULL || files->out == NULL) {;
        return MANAGE_ERROR;
    }
    return MANAGE_OK;
}


FILE* open_file_and_check(char* str_file, const char* mode, int access_mode) {
    FILE* file = fopen(str_file, mode);

    if(access(str_file, access_mode) == NO_ACCESS){
        return NULL;
    }
    return file;
}


void close_files(struct argv_files* files) {
    fclose(files->source);
    fclose(files->out);
    files->source = NULL;
    files->out = NULL;
}
